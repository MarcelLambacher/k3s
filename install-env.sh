#!/bin/bash

mkdir -p ~/.kube
cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
echo "Kubectl connected to K3S"

kubectl create -f rbac-config.yaml
helm init --service-account tiller --history-max 200
echo "HELM and Tiller initialized"