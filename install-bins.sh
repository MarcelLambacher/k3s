#!/bin/bash

chmod +x k3s
chmod +x helm
chmod +x kubectl

cp k3s /usr/local/bin
cp helm /usr/local/bin
cp kubectl /usr/local/bin